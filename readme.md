
# Lämpötilatallennin

Palveluun voidaan tallentaa lämpötiloja paikan perusteella. Tietokantaan luodaan paikka, tai paikkoja, joille voidaan lisätä lämpötiloja. Lämpötilan lisäksi tallennetaan myös aika ja päivämäärä. Jokaiselle paikalle myös lasketaan kaikista niiden sisältämistä lämpötiloista keskilämpötila.  

# API kutsut
## Tiedonhaku:  
GET /api/v1/locations/  
GET /api/v1/locations/#id

 - Hakee kaikki paikat tietokannasta
 - Voidaan rajata haku yhteen paikkaan id-arvon avulla
 - Lisäksi tiedonhaun rajaamiseksi voidaan käyttää seuraavia parametrejä:
   - name
     - Paikannimi joka sisältää annetun tekstin
   - maxTemp
     - Lämpötilat jotka <= annettu arvo
   - minTemp
     - Lämpötilat jotka >= annettu arvo
   - maxAvgTemp
     - Alueet joiden keskilämpötila <= annettu arvo
   - minAvgTemp
     - Alueet joiden keskilämpötila >= annettu arvo
   - maxTime
     - Lämpötilat joiden ottoaika <= annettu arvo
     - Formaatti: hh:mm:ss
   - minTime
     - Lämpötilat joiden ottoaika >= annettu arvo
     - Formaatti: hh:mm:ss
   - maxDate
     - Lämpötilat joiden ottopäivämäärä <= annettu arvo
     - Formaatti: yyyy-mm-dd
   - minDate
     - Lämpötilat joiden ottopäivämäärä >= annettu arvo
     - Formaatti: yyyy-mm-dd
 - Kaikkia parametrejä voi yhdistää vapaasti ja käyttää id:n kanssa.
 - Vastaus tulee JSON-muodossa, HTTP 200-vastauskoodina.

## Tiedon lisäys:
POST /api/v1/locations/add
 - Lisää uuden paikan tietokantaan
 - Parametrit:
   - name
     - Paikannimi
     - Pakollinen
   - info
     - Lisäkuvaus paikasta
     - Optionaalinen
 - HTTP 201-vastauskoodi jos lisäys onnistui, muuten 404

POST /api/v1/temperatures/add
 - Lisää uuden lämpötilatietueen tietokantaan
 - Parametrit:
   - temperature
     - Lämpötilan arvo
     - Pakollinen
   - location_name
     - Paikan johon lämpötila liittyy nimi
     - Pakollinen
     - Nimellä varustettu paikka täytyy löytyä tietokannasta
   - time
     - Ajanhetki
     - Optionaalinen
     - Jos ei annettu, saa sen hetkisen palvelimen ajan
   - date
     - Päivämäärä
     - Optionaalinen
     - Jos ei annettu, saa sen hetkisen palvelimen päivämäärän
 - HTTP 201-vastauskoodi jos lisäys onnistui, muuten 404

## Tiedon muutos:  
PUT /api/v1/locations/update/#id
 - Muuttaa annetun id:n omaavan paikan info osiota
 - Parametrit:
   - info
     - Uusi info-osuus paikalle
     - Pakollinen
 - HTTP 200-vastauskoodi jos muutos onnistui, muuten 404

Lämpötilojen tietoja ei voida lisäyksen jälkeen muuttaa, vaan virheen sattuessa ne on poistettava ja lisättävä uudestaan korjatuilla arvoilla.

## Tiedon poisto:  
DELETE /api/v1/locations/delete/#id
 - Poistaa paikan annetun id:n perusteella
 - HTTP 200-vastauskoodi jos poisto onnistui, muuten 404
 
DELETE /api/v1/temperatures/delete/#id
 - Poistaa lämpötilan annetun id:n perusteella  
 - HTTP 200-vastauskoodi jos poisto onnistui, muuten 404
