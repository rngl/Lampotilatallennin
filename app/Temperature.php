<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class temperature extends Model
{
    protected $table = 'temperature';
    public $timestamps = false;

    // link to location
    public function location() {
        return $this->belongsTo('App\location', 'location_name', 'name');
    }

    // query helpers
    public function scopeBetweenTemperatures($query, $request) {
        $min = $request->query('minTemp', '-500');
        $max = $request->query('maxTemp', '5000');
        return $query->whereBetween('temperature', [$min, $max]);
    }

    public function scopeBetweenTimes($query, $request) {
        $min = $request->query('minTime', '00:00:00');
        $max = $request->query('maxTime', '23:59:59');
        return $query->whereBetween('time', [$min, $max]);
    }

    public function scopeBetweenDates($query, $request) {
        $min = $request->query('minDate', '0001-01-01');
        $max = $request->query('maxDate', '2200-12-30');
        return $query->whereBetween('date', [$min, $max]);
    }

}
