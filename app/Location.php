<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class location extends Model
{
    protected $table = 'location';
    public $timestamps = false;
    // calculated field
    protected $appends = array('avgTemp');

    // link to temperatures, one to many
    public function temperature() {
        return $this->hasMany('App\temperature', 'location_name', 'name');
    }

    // calculate value of 'avgTemp' field
    public function GetAvgTempAttribute() {
        return \App\temperature::where('location_name', '=', $this->name)->avg('temperature');
    }
}
