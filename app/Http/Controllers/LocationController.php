<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;

class LocationController extends Controller
{
    // get location data
    public function getAll($id = '0', Request $request) {
        $result = \App\Location::
            // add temperature fields within given parameters
            with(['temperature' => function($query) use ($request) {
                if (isset($request->minTemp) || isset($request->maxTemp)) {
                    $query->betweenTemperatures($request);
                }
                if (isset($request->minTime) || isset($request->maxTime)) {
                    $query->betweenTimes($request);
                }
                if (isset($request->minDate) || isset($request->maxDate)) {
                    $query->betweenDates($request);
                }
            }])
            // limit results to only those with temperature fields within parameters
            ->when($request, function($query) use ($request) {
                if (isset($request->minTemp) || isset($request->maxTemp)) {
                    $query->whereHas('temperature', function ($q) use ($request) {
                        $q->betweenTemperatures($request);
                    });
                }
                if (isset($request->minTime) || isset($request->maxTime)) {
                    $query->whereHas('temperature', function ($q) use ($request) {
                        $q->betweenTimes($request);
                    });
                }
                if (isset($request->minDate) || isset($request->maxDate)) {
                    $query->whereHas('temperature', function ($q) use ($request) {
                        $q->betweenDates($request);
                    });
                }
            })
            // limit results by location name
            ->when($request->name, function($query) use ($request) {
                return $query->where('name', 'LIKE', "%$request->name%");
            })
            // limit result by id
            ->when($id, function($query) use ($id) {
                return $query->where('id', '=', $id);
            })
            ->get();

        // limit results by average temperature (calculated attribute, not in database)
        if (isset($request->minAvgTemp) || isset($request->maxAvgTemp)) {
            $minAvgTemp = $request->query('minAvgTemp', "-500");
            $maxAvgTemp = $request->query('maxAvgTemp', '5000');
            $modifiedResult = [];

            foreach ($result as $res) {
                if ($res['avgTemp'] <= $maxAvgTemp && $res['avgTemp'] >= $minAvgTemp)
                    array_push($modifiedResult, $res);
            }
            return $modifiedResult;
        }
        return $result;
    }

    // add a location
    public function addLocation(Request $request) {
        // validate 'name' field
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required|unique:location'
        ]);
        // if exists, response 409, else 404
        if ($validator->fails()) {
            if (\App\Location::where('name','=',$request->name)->get())
                abort(409);
            abort(404);
        }

        // save into database
        $location = new \App\Location;
        $location->name = $request->name;
        $location->info = $request->info;
        $location->save();
        // 201, created
        return response()->json('ok', 201);
    }

    // delete a location
    public function deleteLocation($id) {
        $location = \App\Location::findOrFail($id);

        // delete any temperatures on location
        \App\Temperature::where('location_name', '=', $location->name)->delete();
        \App\Location::destroy($id);
    }

    // update a location (change info)
    public function updateLocation($id, Request $request) {
        $location = \App\Location::findOrFail($id);

        // update if new info was provided
        if (isset($request->info)) {
            $location->info = $request->info;
            $location->save();
        }
        else
            abort(404);
    }
}
