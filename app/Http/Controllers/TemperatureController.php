<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class TemperatureController extends Controller
{
    // add temperature
    public function addTemperature(Request $request) {

        // validate fields
        $validator = Validator::make($request->all(), [
            'location_name' => 'bail|required',
            'temperature' => 'bail|required'
        ]);
        if ($validator->fails()) {
            abort(404);
        }

        // add if a location is found in database
        if (\App\location::where('name', '=', $request->location_name)->exists()) {
            $temperature = new \App\Temperature;
            $temperature->temperature = $request->temperature;
            $temperature->location_name = $request->location_name;
            // if no time or date specified, use current date and time of server
            $temperature->time = $request->query('time', date('H:i:s'));
            $temperature->date = $request->query('date', date('Y-m-d'));
            $temperature->save();
            // 201, created
            return response()->json('ok', 201);
        }
        else
            abort(404);
    }

    // delete temperature
    public function deleteTemperature($id) {
        \App\Temperature::destroy($id);
    }
}
