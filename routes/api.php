<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/v1/locations/{id?}', 'LocationController@getAll');
Route::get('/v1', function() {return view('info');});
Route::post('/v1/locations/add', 'LocationController@addLocation');
Route::delete('/v1/locations/delete/{id}', 'LocationController@deleteLocation');
Route::put('/v1/locations/update/{id}', 'LocationController@updateLocation');

Route::post('/v1/temperatures/add', 'TemperatureController@addTemperature');
Route::delete('/v1/temperatures/delete/{id}', 'TemperatureController@deleteTemperature');