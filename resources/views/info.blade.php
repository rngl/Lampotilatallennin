<?php
/**
 * Created by PhpStorm.
 * User: rngl
 * Date: 10.3.2018
 * Time: 23:21
 */

echo ("<title>Quick guide</title>
<h1>
GET /v1/locations <br>
GET /v1/locations/#id <br></h1>
 <h2> - Params: minTemp, maxTemp, minTime, maxTime, minDate, maxDate, minAvgTemp, maxAvgTemp, name
 <br> - time format: hh:mm:ss
 <br> - date format: yyyy-mm-dd</h2>
<h1>
POST /v1/locations/add<br></h1>
<h2> - Params: name(required), info
</h2>
<h1>
POST /v1/temperatures/add<br></h1>
<h2> - Params: temperature(required), location_name(required), time, date
</h2>
<h1>
PUT /v1/locations/update/#id<br>
</h1>
<h2> - Params: info(required)
<h1>
DELETE /v1/locations/delete/#id<br>
DELETE /v1/temperatures/delete/#id
</h1>
");
